# vim:ft=zsh

################################################################################
## Automatically start tmux irc session

if (( $+commands[tmux] )) && [[ -z "$TMUX" ]]; then
  exec tmux new-session -A -s weechat weechat
fi
