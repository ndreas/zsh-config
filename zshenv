# vim:ft=zsh

################################################################################
## Exports

# Disable Nixos' system compinit
export NOSYSZSHRC_COMPINIT=1

# Language
export LANG=en_US.UTF-8

# Editor
if (( $+commands[nvim] )); then
  export EDITOR="nvim"
elif (( $+commands[vim] )); then
  export EDITOR="vim"
fi

# Pager
export PAGER="$(echo =less) -R --quit-if-one-screen --no-init"
if (( $+commands[bat] )); then
  export MANROFFOPT="-c"
  export BAT_PAGER="$PAGER"
  export MANPAGER="sh -c 'col -bx | bat -l man -p'"
else
  export MANPAGER="$EDITOR -c 'set ft=man' -"
fi

# XDG config
export XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"

if [[ -d "$XDG_RUNTIME_DIR" ]]; then
  if [[ -e "$XDG_RUNTIME_DIR/keyring/ssh" ]]; then
    # Gnome keyring for ssh
    export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/keyring/ssh"
  elif [[ -e "$XDG_RUNTIME_DIR/ssh-agent.socket" ]]; then
    # Regular ssh-agent
    export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
  fi
fi

# Golang
export GO111MODULE=on
export GOPATH=$HOME/.gopath

# ripgrep rc
if [[ -f "$XDG_CONFIG_HOME/ripgrep/ripgreprc" ]]; then
  export RIPGREP_CONFIG_PATH="$XDG_CONFIG_HOME/ripgrep/ripgreprc"
fi


################################################################################
## Path

# Make the path unique
typeset -U path

# Home bin
path+=(
  # home dir
  $HOME/bin
  $HOME/.local/bin
  # rust cargo
  $HOME/.cargo/bin
  # golang binaries
  $GOPATH/bin
  # npm binaries
  $HOME/.cache/npm/global/bin
)


################################################################################
## Host-local configuration hook

[[ -f $HOME/.zshenv-local ]] && source $HOME/.zshenv-local || true


################################################################################
## Backup path if there is anything after .zshenv that resets the path

export BACKUP_PATH=$PATH
