# vim:ft=zsh

################################################################################
## Restore the path if .zshenv has been overridden

if [ $PATH != $BACKUP_PATH ]; then
  export PATH=$BACKUP_PATH;
fi


################################################################################
## Host-local configuration hook

test -f $HOME/.zprofile-local && source $HOME/.zprofile-local
