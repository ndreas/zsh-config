# vim:ft=zsh
# zmodload zsh/zprof

if [[ ! -f "${HOME}/.local/share/znap/zsh-snap/znap.zsh" ]]; then
  mkdir -p "${HOME}/.local/share/znap"
  git clone --depth 1 -- https://github.com/marlonrichert/zsh-snap.git \
    "${HOME}/.local/share/znap/zsh-snap"
fi

source ~/.local/share/znap/zsh-snap/znap.zsh

################################################################################
# Host-local configuration hook

[[ -f $HOME/.zshrc-local-before ]] && source $HOME/.zshrc-local-before || true

################################################################################
## auto-tmux on ssh

if [[ -v SSH_CLIENT && "$TERM" != tmux-* && -z "$TMUX" ]] && (( $+commands[tmux] )); then
  exec tmux new-session
fi

################################################################################
## NixOS

# Remove compinit flag since it shouldn't be carried to sudo shells
unset NOSYSZSHRC_COMPINIT

################################################################################
## Paths

# Add home and parent directory to cd completion
cdpath=($HOME ..)

# Add config dir to function path
fpath+=($HOME/.config/zsh/functions)

# Add local completion dir to function path
fpath+=($HOME/.local/share/zsh/completions)

################################################################################
## Options

# Automatic pushd
setopt AUTO_PUSHD

# Disable beeping
setopt NO_BEEP

# Terminal displays combining characters correctly
setopt COMBINING_CHARS

# Correct spelling of commands
setopt CORRECT

# Treat #, ~, ^ as part of globbing patterns
setopt EXTENDED_GLOB

# Don't send HUP to running jobs on shell exit
setopt NO_HUP

# Allow parameter expansion, command substitution and arithmetic expansion in
# the prompt
setopt PROMPT_SUBST

# Disable C-Q/C-S
setopt NO_FLOW_CONTROL

# Allow comments in shell
setopt INTERACTIVE_COMMENTS

# Show PID when suspending jobs
setopt LONG_LIST_JOBS

# Enable Emacs/Readline keybindings, do this early to allow later overrides
bindkey -e

################################################################################
## History

# Increase saved history
HISTSIZE=1000000
SAVEHIST=$HISTSIZE

# History file
HISTFILE=$HOME/.zsh_history

# Don't show EOL marks
PROMPT_EOL_MARK=''

# Support bang commands for expanding history
setopt BANG_HIST

# Add timestamps to history
setopt EXTENDED_HISTORY

# Disable beeping
setopt NO_HIST_BEEP

# Ignore duplicate commands
setopt HIST_IGNORE_ALL_DUPS

# Prefix a command with a space to exclude it from history
setopt HIST_IGNORE_SPACE

# Remove the history command from the history
setopt HIST_NO_STORE

# Append commands to history immediately rather than replacing the history,
# allowing parallel sessions
setopt INC_APPEND_HISTORY

# Redefine word characters
WORDCHARS=${WORDCHARS/\//}

################################################################################
## Prompt

if [[ "$PROMPT" != *starship* ]]; then
  if (( $+commands[starship] )); then
    eval "$(starship init zsh)"
  else
    function () {
      local user_segment='%(!.%F{red}.%F{green})%n'
      local path_segment=' %F{8}• %F{blue}%2~'
      local result_segment='%(?.. %F{8}• %F{red}%? •)'
      local prompt_segment='%(1j.%F{magenta}%j .)%B%F{blue}➜%b%f '
      PROMPT=$'\n'${user_segment}${path_segment}${result_segment}$'\n'${prompt_segment}
    }
  fi
fi

################################################################################
## Aliases

# ls setup
if (( $+commands[eza] )); then
  if [[ -z "$EZA_COLORS" ]]; then
    export EZA_COLORS="di=1;38;5;12:ex=1;38;5;10:so=38;5;9:bd=38;5;11:cd=38;5;11:uu=38;5;11:gu=38;5;11:df=38;5;10"
    alias ls='eza --group-directories-first --classify=auto'
  fi
elif [ "$OSTYPE" = "linux-gnu" ]; then
  # Enable GNU ls colors if available
  alias ls='ls -Fh --color'
  # Use dircolors if available for $LS_COLORS
  (( $+commands[dircolors] )) && eval $(dircolors)
else
  alias ls='ls -Fh'
  # Enable colors in BSD ls
  export CLICOLOR=1
  # Colors for BSD ls
  export LSCOLORS=ExGxFxdaCxDaDahbadacec
fi

# Rustified tools
(( $+commands[dust] )) && alias du=dust
(( $+commands[btm] )) && alias top=btm

# Interactive mv and cp
alias mv='mv -iv'
alias cp='cp -iv'

# Pop directory from stack
alias pd='popd'

# Show last command duration
alias dur='fc -nliLID -1'

# Colors
alias grep='grep --color -I'
alias ip='ip --color=auto'

# Vim
if (( $+commands[nvim] )); then
  alias vi=nvim
  alias vim=nvim
fi

# Pager setup
if (( $+commands[bat] )); then
  alias cat='bat'
  alias less='bat'
elif (( $+PAGER )); then
  alias less=$PAGER
fi

################################################################################
## zstyles

# Completion cache
zstyle ':completion:*' cache-path ~/.cache/zsh/completion
zstyle ':completion:*' use-cache on

# Display match groups separately
zstyle ':completion:*' group-name ''

# Scroll completion lists that don't fit the screen
zstyle ':completion:*' list-prompt "%F{8}Line %L tab for next%f"

# Use adapted dircolors for completion list colors
# dircolors + 01;X -> 38;5;X-22 for conversion of bold to bright
local list_colors='rs=0:di=38;5;12:ln=38;5;14:mh=00:pi=40;33:so=38;5;13:do=38;5;13:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=38;5;10:*.tar=38;5;9:*.tgz=38;5;9:*.arc=38;5;9:*.arj=38;5;9:*.taz=38;5;9:*.lha=38;5;9:*.lz4=38;5;9:*.lzh=38;5;9:*.lzma=38;5;9:*.tlz=38;5;9:*.txz=38;5;9:*.tzo=38;5;9:*.t7z=38;5;9:*.zip=38;5;9:*.z=38;5;9:*.dz=38;5;9:*.gz=38;5;9:*.lrz=38;5;9:*.lz=38;5;9:*.lzo=38;5;9:*.xz=38;5;9:*.zst=38;5;9:*.tzst=38;5;9:*.bz2=38;5;9:*.bz=38;5;9:*.tbz=38;5;9:*.tbz2=38;5;9:*.tz=38;5;9:*.deb=38;5;9:*.rpm=38;5;9:*.jar=38;5;9:*.war=38;5;9:*.ear=38;5;9:*.sar=38;5;9:*.rar=38;5;9:*.alz=38;5;9:*.ace=38;5;9:*.zoo=38;5;9:*.cpio=38;5;9:*.7z=38;5;9:*.rz=38;5;9:*.cab=38;5;9:*.wim=38;5;9:*.swm=38;5;9:*.dwm=38;5;9:*.esd=38;5;9:*.jpg=38;5;13:*.jpeg=38;5;13:*.mjpg=38;5;13:*.mjpeg=38;5;13:*.gif=38;5;13:*.bmp=38;5;13:*.pbm=38;5;13:*.pgm=38;5;13:*.ppm=38;5;13:*.tga=38;5;13:*.xbm=38;5;13:*.xpm=38;5;13:*.tif=38;5;13:*.tiff=38;5;13:*.png=38;5;13:*.svg=38;5;13:*.svgz=38;5;13:*.mng=38;5;13:*.pcx=38;5;13:*.mov=38;5;13:*.mpg=38;5;13:*.mpeg=38;5;13:*.m2v=38;5;13:*.mkv=38;5;13:*.webm=38;5;13:*.webp=38;5;13:*.ogm=38;5;13:*.mp4=38;5;13:*.m4v=38;5;13:*.mp4v=38;5;13:*.vob=38;5;13:*.qt=38;5;13:*.nuv=38;5;13:*.wmv=38;5;13:*.asf=38;5;13:*.rm=38;5;13:*.rmvb=38;5;13:*.flc=38;5;13:*.avi=38;5;13:*.fli=38;5;13:*.flv=38;5;13:*.gl=38;5;13:*.dl=38;5;13:*.xcf=38;5;13:*.xwd=38;5;13:*.yuv=38;5;13:*.cgm=38;5;13:*.emf=38;5;13:*.ogv=38;5;13:*.ogx=38;5;13:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:';
zstyle ':completion:*' list-colors "${(s.:.)list_colors}"

# Case insensitive
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# Prompt for menu selection
zstyle ':completion:*' select-prompt "%F{8}Match: %m%f"

# Ignore self and parent already in path when completing directories
zstyle ':completion:*:(cd|mv|cp):*' ignore-parents parent pwd

# Display a menu when completing kill
zstyle ':completion:*:kill:*' force-list always
zstyle ':completion:*:*:kill:*' menu yes select

# Kill process completion
if (( $EUID != 0 )); then
  # Complete all the user's processes for kill
  zstyle ':completion:*:processes' command 'ps -x -o pid,ppid,user,command'
else
  # Complete all processes
  zstyle ':completion:*:processes' command 'ps -a -x -o pid,ppid,user,command'
fi

# Color for kill
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'

# Ignore binary file types for vim
zstyle ':completion:*:(n|m|g|)vim:*' ignored-patterns '*.(o|a|so|aux|dvi|swp|fig|bbl|blg|bst|idx|ind|out|toc|class|pdf|ps|lock)'

# Ignore already present arguments for rm
zstyle ':completion:*:rm:*' ignore-line yes

# Autocomplete settings
zstyle ':autocomplete:*' min-delay 0.5
zstyle ':autocomplete:key-bindings' enabled no
zstyle ':autocomplete:recent-dirs' enabled no

################################################################################
## Plugins

# Disable overwriting of dir stack
+autocomplete:recent-directories() {
  reply=()
}

znap prompt

autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic

autoload -Uz rm
autoload -Uz mkscript
autoload -Uz zmv

znap source ndreas/zsh-autocomplete
znap source Aloxaf/fzf-tab
znap source zsh-users/zsh-autosuggestions
znap source zsh-users/zsh-completions
znap source zsh-users/zsh-syntax-highlighting

zstyle ':completion:*:descriptions' format '[%d]'

export ZSH_AUTOSUGGEST_USE_ASYNC=1
export ZSH_AUTOSUGGEST_MANUAL_REBIND=1
bindkey '^Y' autosuggest-accept

if (( $+commands[zoxide] )); then
  eval "$(zoxide init zsh --no-aliases)"
  autoload -Uz z
fi

if (( $+commands[direnv] )); then
  eval "$(direnv hook zsh)"
  export DIRENV_LOG_FORMAT="[30m%s[00m"
fi

# History
if (( $+commands[atuin] )); then
  (( $+functions[_atuin_search] )) && eval "$(atuin init zsh)"
elif (( $+commands[fzf] )); then
  autoload -Uz fzf-history-widget
  zle -N fzf-history-widget
  bindkey '^R' fzf-history-widget
else
  bindkey '^R' history-incremental-search-backward
fi

# fzf
if (( $+commands[fzf] )); then
  export _ZO_FZF_OPTS="${FZF_DEFAULT_OPTS} --tiebreak=index --height=8 --info=inline --reverse"

  if [[ ! -z "$TMUX" ]]; then
    autoload -Uz fzf-tmux-widget
    zle -N fzf-tmux-widget
    bindkey '^T' fzf-tmux-widget
  fi
fi


################################################################################
# Hooks - needs to run after znap

autoload -Uz add-zsh-hook
autoload -Uz add-zle-hook-widget

hook-title-current-dir() {
  # Set the terminal title to the current directory
  print -Pn "\e]0;%(5~|…/%3~|%4~)\a"
}
add-zsh-hook precmd hook-title-current-dir

hook-title-command() {
  # Set the terminal title to the command
  print -n "\e]0;$2\a"
}
add-zsh-hook preexec hook-title-command

hook-cursor-line() {
  # line cursor: \e[5 q
  echo -ne '\e[5 q'
}
zle -N hook-cursor-line
add-zle-hook-widget line-init hook-cursor-line

hook-cursor-block() {
  # block cursor: \e[1 q
  echo -ne '\e[1 q'
}
zle -N hook-cursor-block
add-zle-hook-widget line-finish hook-cursor-block

hook-cursor-vimode() {
  case "${KEYMAP}" in
    viins | main)
      echo -ne '\e[5 q'
      ;;
    *)
      echo -ne '\e[1 q'
      ;;
  esac
}
zle -N hook-cursor-vimode
add-zle-hook-widget keymap-select hook-cursor-vimode


################################################################################
## Key bindings

# Switch to vi normal mode
bindkey '^[' vi-cmd-mode

# Allow shift-tab to revers the direction
bindkey '^[[Z' reverse-menu-complete

# Run help
bindkey -r '^Xh'
bindkey '^Xh' run-help

# History
bindkey '^[[A' up-line-or-history
bindkey '^P' up-line-or-history
bindkey '^[[B' down-line-or-history
bindkey '^N' down-line-or-history


################################################################################
# Host-local configuration hook

[[ -f $HOME/.zshrc-local ]] && source $HOME/.zshrc-local || true

# zprof
