#!/bin/sh
force=$1
zsh_dir=$(CDPATH='' cd -- "$(dirname "$0")" && pwd)

cd "$HOME" || exit 1

symlink_file() {
  file=$1

  if [ ! -f "$file" ]; then
    echo "${file} not found, skipping"
    return
  fi

  target=$2

  if [ -f "$target" ] && [ "$force" != "-f" ]; then
    echo "${HOME}/${target} exists, skipping, use -f to overwrite"
    return
  fi

  echo "Linking ${HOME}/${target} to ${file}"
  ln -sf "$file" "$target"
}

host=$(hostname -s)

mkdir -p ~/.cache/zsh

symlink_file "${zsh_dir}/zprofile"             .zprofile
symlink_file "${zsh_dir}/zshenv"               .zshenv
symlink_file "${zsh_dir}/zshrc"                .zshrc
symlink_file "${zsh_dir}/${host}.zshenv"       .zshenv-local
symlink_file "${zsh_dir}/${host}.zshrc"        .zshrc-local
symlink_file "${zsh_dir}/${host}.before.zshrc" .zshrc-local-before
